$(document).ready(function() {
    $('.accordion_title').click(function() {
        
        // if($(this).parent().hasClass('active')) {
        //     $(this).slideUp('active');
        // } else {
        //     $(this).slideDown('active');
        // }
        $(this).parent().find('.accordion__content').slideToggle(400, 'swing');
    });

    $('.coming-soon').click(function() {
        $('.popup').css({display: "flex"}).hide().fadeIn(500);
    });

    $('.popup').click(function (event) {
        if(!$(event.target).closest('#modalBox').length > 0 && !$(event.target).is('#modalBox')) {
            $(".popup").fadeOut(function() {
                $(this).css({display: "none"});
            });
        }     
    });
    
    // $('#ambassador-link').click(function() {
    //     window.location.href = 'https://forms.gle/z9Yv9gMo3rdLxkBR7';
    // })

});
// $(window).on("load", function() {
//     $('.popup').css({display: "flex"}).hide().fadeIn();
// });